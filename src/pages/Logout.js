import {useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){

	const {unsetUser, setUser} = useContext(UserContext);

	// clear the localStorage of the user's information
	unsetUser()
	// localStorage.clear();

	// placing the "setUser" function inside of a useEffect is necessary because of updates within ReactJS that a state of another component cannot be updated while trying to render a different component

	// by adding useEffect, this will allow the logout page to render first before triggering the useEffect which changes the state of the user
	useEffect(() => {

		// set the user state back to its original state
		setUser({id: null})
	})

	return(
		<Navigate to="/login"/>

	)
}