import {Link} from 'react-router-dom';
import {Col} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';

export default function Error(){

	return (
	<Col className ="p-5">
		<h1>404 - Not Found</h1>
		<p>The page you are looking for cannot be found!</p>
		<Link to="/" className="btn btn-primary">Back Home</Link>
	</Col>
	)
}

