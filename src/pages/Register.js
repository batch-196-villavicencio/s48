import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';

import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';



// const [getter, setter] = useState(initialGetterValue);
export default function Register(){

	const {user} = useContext(UserContext);
	const history = useNavigate();

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobile, setMobileNo] = useState('')
	const [email, setEmail] = useState('')
	const [pass1, setPass1] = useState('')
	// const [pass2, setPass2] = useState('')
	const [isActive, setIsActive] = useState(false)

	// console.log(email);
	// console.log(pass1);
	// console.log(pass2);

	function registerUser(e){
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmailExists',{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
					title: "Email is already in use",
					icon: "info",
					text: "The email you are trying to register already exists in our database"
				});
			} else {
				fetch('http://localhost:4000/users',{
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobile,
						email: email,
						password: pass1
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if(data){
						Swal.fire({
							title: 'Registration Successful!',
							icon: 'success',
							text: 'Thank you for registering'
						});

						history("/login");
					} else {
						Swal.fire({
							title: 'Registration Failed',
							icon: 'error',
							text: 'Something went wrong. Please try again later'
						});
					};
				})
			}
		})

		// clear input fields
		setEmail('');
		setPass1('');
		setFirstName('');
		setLastName('');
		setMobileNo('');

		// alert('Thank you for registering');
	}

	// Syntax:
		// useEffect(() => {}, [])

	useEffect(() => {
		
		if (email !== '' && pass1 !== '' && firstName !== '' && lastName !== '' && mobile !== '' && mobile.length === 11) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, pass1, firstName, lastName, mobile]);

	return(
		(user.id !== null)?
			<Navigate to="/courses"/>
		:
		<>
		<h1>Register Here:</h1>
		<Form onSubmit={e => registerUser(e)}>
			<Form.Group controlId="userFirstName">
				<Form.Label className="mt-3">First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your first name"
					required
					value= {firstName}
					onChange= {e => setFirstName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="userLastName">
				<Form.Label className="mt-3">Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your last name"
					required
					value= {lastName}
					onChange= {e => setLastName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="userMobile">
				<Form.Label className="mt-3">Mobile No.</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your mobile number"
					required
					value= {mobile}
					onChange= {e => setMobileNo(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label className="mt-3">Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value= {email}
					onChange= {e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label className="mt-2">Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here"
					required
					value= {pass1}
					onChange= {e => setPass1(e.target.value)}
				/>
			</Form.Group>


			{ isActive ?
				<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">
							Register
				</Button>
				:
				<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>
							Register
				</Button>
			}
		</Form>
		</>
	)
}

