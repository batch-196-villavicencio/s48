import {useState, useEffect, useContext} from 'react';
import {Navigate, Link} from 'react-router-dom'
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login(){

	// Allows us to consume the User context object and its properties to use for user validation
	const {user, setUser} = useContext(UserContext);
	// console.log(user)

	const [email, setEmail] = useState('')
	const [pass, setPass] = useState('')
	const [isActive, setIsActive] = useState(false)

	function loginUser(e){
		e.preventDefault();

		//Syntax:
			// fetch:('url', options)
		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: pass
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Booking App of 196!"
				})
			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your credentials"
				})

			}
		})

		// Set the email of the authenticated user in the local storage
			// Syntax:
				// localStorage.setItem('propertyName/key', value);
		// localStorage.setItem("email", email);

		// access user information through localStorage to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout

		// when states change components are rerendered and the AppNavbar component will be updated based on the credentials.

		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		setEmail('');
		setPass('');

		// alert(`${email} has successfully logged in`);
	}

	const retrieveUserDetails = (token) => {

		fetch('http://localhost:4000/users/getUserDetails', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}


	useEffect(() => {
		if(email !== '' && pass !== '') {
			setIsActive(true);
		} else {
			setIsActive(false)
		}
	}, [email, pass]);

	return(

		(user.id !== null)?
			<Navigate to="/courses"/>
		:
		<>
		<h1>Login</h1>
		<Form onSubmit={e => loginUser(e)}>
			<Form.Group controlId="userLoginEmail">
				<Form.Label>
					Email Address
				</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value={email}
					onChange= {e => setEmail(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="pass">
				<Form.Label className="mt-2">
					Password
				</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					required
					value={pass}
					onChange= {e => setPass(e.target.value)}
				/>

			</Form.Group>
				<p>Not yet registered? <Link to="/register">Register Here</Link></p>

				{ isActive ?
					<Button className="mt-2 mb-5" variant="success" type="submit" id="submitLoginBtn">
						Login
					</Button>
					:
					<Button className="mt-2 mb-5" variant="danger" type="submit" id="submitLoginBtn">
						Login
					</Button>
				}

		</Form>
		</>


	)
}