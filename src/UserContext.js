import React from 'react';


//Creates a Context Object
//context object- data type of an object that can be used to store information that can be shared to other components within the app
const UserContext = React.createContext();

// provider component allows other components to consume/use the context object and supply the necessary information needed
export const UserProvider = UserContext.Provider;

export default UserContext;