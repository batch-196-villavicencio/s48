import {Container} from 'react-bootstrap';
import {useState, useEffect} from 'react'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import CourseView from './components/CourseView';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Courses from './pages/Courses';
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'
import './App.css';
import {UserProvider} from './UserContext'

function App() {  

	//state hook for the user state that defined here for a global scope
	//initialized as an object with properties from the localStorage
	//this will be used to store the user information and will be used for validating if a user is logged in on the app or not
	const [user, setUser] = useState({
		id: null,
		isAdmin: null
	});

	const unsetUser = () => {
		localStorage.clear();
	};

	useEffect(() => {
		fetch('http://localhost:4000/users/getUserDetails', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// captured the data of whoever is logged in
			// console.log(data);

			if(typeof data._id !== "undefined"){
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			} else {
				// set back the initial state of user
				setUser({
					id: null,
					isAdmin: null
				})
			}

		});

	},[]);


	return(
		<>
			<UserProvider value={{user, setUser, unsetUser}}>
				<Router>
					<AppNavbar/>
					<Container>
						<Routes>
							<Route exact path= "/" element={<Home/>}/>
							<Route exact path= "/courses" element={<Courses/>}/>
							<Route exact path= "/courseView/:courseId" element={<CourseView/>}/>
							<Route exact path= "/register" element={<Register/>}/>
							<Route exact path= "/login" element={<Login/>}/>
							<Route exact path= "/logout" element={<Logout/>}/>
							<Route exact path= "*" element={<Error/>}/>
						</Routes>
					</Container>
				</Router>
			</UserProvider>
		</>
	)
}

export default App;
