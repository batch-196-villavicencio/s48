// import {useState} from 'react'
import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}){
	// object destructuring
	const {_id, name, description, price} = courseProp

	// react hooks- useState -> store its state
	// Syntax:
		// const [getter, setter] = useState(initialGetterValue);
	// const [count, setCount] = useState(0);
	// const [a, setCount2] = useState(10);
	// // console.log(useState(0));

	// function enroll(){
	// 	if (count === 10 || a === 0){
	// 		return alert("No more seats available")
	// 	}
	// 	setCount(count + 1);
	// 	setCount2(a - 1);
	// 	// console.log(`Enrollees: ${count}`);
	// }

	return(
			<Card className="cardHighlight p-3 mb-3">
				<Card.Body>
					<Card.Title className="fw-bold">
						{name}
					</Card.Title>
					<Card.Subtitle>Course Description</Card.Subtitle>
					<Card.Text>
						{description}
					</Card.Text>
					<Card.Subtitle>Course Price</Card.Subtitle>
					<Card.Text>
						{price}
					</Card.Text>
					<Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
				</Card.Body>
			</Card>
	)
}