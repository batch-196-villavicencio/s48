import {Link} from 'react-router-dom';
import {Row, Col} from 'react-bootstrap';

export default function Banner(){
	return (
		<Row>
			<Col className ="p-5">
				<h1>B196 Booking App</h1>
				<p>Opportunities for everyone, everywhere!</p>
				<Link to="/courses" className="btn btn-primary">Enroll Now</Link>
			</Col>
		</Row>
	)
};