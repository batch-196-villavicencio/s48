import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
// import AppNavbar from './components/AppNavbar';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = "Tony Parker";
// const element = <h1> Hello, {name}</h1>

// const user = {
//   firstName: 'Mikasa',
//   lastName: 'Ackerman'
// };

// const formatName = (a) => {
//   return a.firstName + ' ' + a.lastName;
// };

// const fullName = <h1>Hello, {formatName(user)}</h1>

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(fullName);